
const data: string[] = ['un truc', 'un autre truc'];


const form = document.querySelector<HTMLFormElement>('form');
const divTarget = document.querySelector<HTMLDivElement>('#target');
const inputContent = document.querySelector<HTMLInputElement>('#content');

draw();

/**
 * On ajoute un event au submit du formulaire dans lequel on lui dit que si
 * l'input contient bien quelque chose, alors on ajoute un élément à la liste et
 * on regénère l'affichage.
 */
form?.addEventListener('submit', (event) => {
    event.preventDefault(); //Permet d'empêcher le formulaire de recharger la page

    if (inputContent?.value) {
        data.push(inputContent.value);
        
        draw();
    }
});

/**
 * Fonction permettant de supprimer un élément de la liste puis de regénérer l'affichage
 * @param index l'index de l'élément qu'on veut supprimer du tableau (il serait préférable d'utiliser un id et un filter plutôt que l'index)
 */
function removeItem(index:number) {
    data.splice(index, 1);
    draw();
}

/**
 * Fonction dont l'objectif est de générer l'affichage, les balises HTML, en se 
 * basant sur une liste de données. Cette fonction devra être rappelée à chaque modification
 * de la liste/des données
 */
function draw() {
    divTarget!.innerHTML = '';


    for (const item of data) {

        const para = document.createElement('p');

        para.textContent = item;

        divTarget?.append(para);


        //Partie qui permet de rajouter un bouton de suppression au paragraphe
        const rmBtn = document.createElement('button');
        rmBtn.textContent = 'X';
        para.append(rmBtn);
        rmBtn.addEventListener('click', () => {
            removeItem(data.indexOf(item));
        });
    }
}
