
export type Person = {
    id:number;
    name:string;
    age:number;
}

export type Task = {
    label:string;
    done:boolean;
}