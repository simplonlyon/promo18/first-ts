const para2 = document.querySelector<HTMLElement>('#para2');

//Avec le !, on force typescript à considérer qu'il y a un élément dans para2,
// et si c'est pas le cas, le programme plantera
para2!.style.color = 'blue';

//Alternativement, on peut faire un if pour voir si l'élément existe, pour être sûr que ça plante pas
if(para2) {
    para2.style.color = 'blue';
    //para2.style.setProperties('color','blue');
    // para2.setAttribute('style', 'color:blue');

    para2.textContent = 'Modified by JS';
}

const section2 = document.querySelector<HTMLElement>('#section2');

section2!.style.border = 'dotted';



const colorful = document.querySelector<HTMLElement>('#section2 .colorful');
colorful!.style.backgroundColor = 'orange';

const h2Sect1 = document.querySelector<HTMLElement>('#section1 h2');
h2Sect1!.style.fontStyle = 'italic';

const colorfulInPara = document.querySelector<HTMLElement>('p .colorful');
colorfulInPara!.hidden = true;
// colorfulInPara!.style.display = 'none';
// colorfulInPara!.style.visibility = 'hidden';

const a = document.querySelector<HTMLLinkElement>('#section1 a');
// a!.setAttribute('href', 'https://www.simplonlyon.fr');
a!.href = 'https://www.simplonlyon.fr';

const h2Sect2 = document.querySelector<HTMLElement>('#section2 h2');
h2Sect2!.classList.add('big-text')
