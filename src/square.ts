const playground = document.querySelector<HTMLElement>('#playground');
const square = document.querySelector<HTMLElement>('#square');


playground?.addEventListener('click', (event) => {
    console.log(event.clientX + '-' + event.clientY);

    square!.style.top = event.clientY +'px';
    square!.style.left = event.clientX +'px';
    
});