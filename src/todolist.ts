import { Task } from "./entities";

let list:Task[] = [];


while(true) {
    const input = prompt('What do you need to do ?');
    
    if(input) {
        addTask(input);
    } 
    
    document.body.innerHTML = displayList();
}

/**
 * Fonction rajoutant une task dans la liste de Task
 * @param label Le label de la task à rajouter dans la liste
 */
function addTask(label:string){
    list.push({
        label:label,
        done: false
    });
}

/**
 * Fonction qui boucle sur les task pour créer l'affichage de la liste
 * @returns Une chaîne de caractère représentant la liste de Task
 */
function displayList():string {
    let display = '';
    for (const task of list) {
        if(task.done) {
            display += '\u2713 '+task.label;
        } else {
            display += '\u2717 '+task.label;

        }
        display += '<br>';
    }
    return display;
}

/**
 * Passe une task de terminé à non terminée ou l'inverse
 * @param index l'index de la task à cocher/décocher
 */
function toggleDone(index:number) {
    list[index].done = !list[index].done;
}