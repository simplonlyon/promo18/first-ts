const incrementBtn = document.querySelector<HTMLButtonElement>('#increment');
const decrementBtn = document.querySelector<HTMLButtonElement>('#decrement');
const spanCounter = document.querySelector<HTMLSpanElement>('#counter');

let counter = 0;

incrementBtn?.addEventListener('click', () => {
    counter++;
    spanCounter!.textContent = String(counter);
});

decrementBtn?.addEventListener('click', () => {
    counter--;
    spanCounter!.textContent = String(counter);
});

