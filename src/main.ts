
let toGuess: number = Math.floor(Math.random() * 10);

let isGame: boolean = true;

while (isGame) {
    let input = Number(prompt('What number is it ?'));

    if (input == toGuess) {
        isGame = false;
        console.log('You got it');
    } else if (input > toGuess) {
        console.log('Too big');

    } else if (input < toGuess) {
        console.log('too low');

    } else {
        console.log('Wrong value, please input a number');
    }
}
