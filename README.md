# first-ts



## How To use
1. Cloner le projet
2. Ouvrir un terminal dedans et exécuter `npm i`
3. Exécuter `npm run dev` pour lancer le projet et aller sur http://localhost:3000

## Exercices
### Exo Guess a number ([html](index.html)/[ts](src/main.ts))
Dans le main.ts, faire un guess a number
Exemple de récupération d'input utilisateurice : const input = prompt('question ?');
1. Créer une variable toGuess qui contiendra le nombre à deviner, pour l'instant on peut mettre un nombre en dur
2. Ensuite on peut créer une autre variable isGame booléenne qui contient true par défaut
3. Faire une boucle while qui va tourner tant que isGame
4. Dans la boucle, faire un prompt pour récupérer un input utilisateur (ici, qu'est-ce qu'on pense qu'est le nombre à deviner)
5. Ensuite, on fait une condition pour vérifier si ce qui a été proposé correspond à la variable toGuess, si oui, on passe le isGame à false et on met un ptit console log de bravo, si non on met juste un ptit message que c'est pas ça
⚠️ Il va falloir convertir l'input en number, pour ça on peut utiliser google ou bien : Number(prompt('question'));
Bonus : Faire en sorte que plutôt que juste gagné perdu, ça nous dise si c'est plus ou si c'est moins, et aussi que ça nous engueule si jamais l'input n'est pas un number 


### Exo Todo List rapide en TS  ([html](todolist.html)/[ts](src/todolist.ts))
1. Créer un fichier todolist.html et créer un fichier src/todolist.ts, et charger ce fichier dans le html (comme c'est fait dans le index.html pour le main.ts)
2. Dans le fichier src/entities.ts (ou un autre fichier, peu importe), créer et exporter un type Task qui, comme on l'avait fait en java, va posséder une propriété label de type string et une propriété done en boolean
3. Dans le fichier todolist.ts, créer une variable list qui contiendra un array de task (Task[]) et l'initialiser en tableau vide
4. Créer une fonction addTask qui va attendre en argument un label de type string et qui va pusher un objet task dans la list, avec son done à false
5. Créer une fonction toggleDone(index:number) qui va récupérer la valeur à l'index donné dans la list et qui va passer la valeur de done à true si elle est false et inversement
6. Faire un while qui va utiliser un prompt pour récupérer une valeur et la donner à manger à la fonction addTask
7. Faire une fonction displayList qui va faire une boucle sur la list et afficher avec un console log chaque task avec si elle est terminée ou pas 


### Exercice manipulation DOM ([html](dom.html)/[ts](src/dom.ts))
1. Créer un fichier dom.html et un fichier src/dom.ts et les lier ensemble
2. Mettre ceci dans le HTML : 
    ```html
    <section id="section1">
            <h2>Section 1 Title</h2>
            <p>Some text in the first section <span class="colorful">with text in span</span> coucou</p>
            <a href="#">See more</a>
        </section>

        <section id="section2">
            <h2 class="colorful">Section 2 title</h2>
            <p id="para2">Some text in the second section</p>
        </section>
    ```
3. Et voilà une liste de truc à faire via le typescript en utilisant des querySelector :
    * Mettre le texte de l'élément avec l'id para2 en bleu
    * Mettre une border en pointillet noire à la section2
    * Mettre une background color orange à l'élément de la classe colorful de la section2
    * Mettre le h2 de la section1 en italique
    * Cacher l'élément colorful situé dans un paragraphe
    * Changer le texte de para2 pour "modified by JS"
    * Changer le href du lien de la section1 pour le faire aller sur simplonlyon.fr
    * Rajouter la classe big-text sur le h2 de la section2
    * Bonus : Faire que tous les paragraphe du document soit en italique

### Faire un petit compteur en TS ([html](event.html)/[ts](src/event.ts))
1. Soit dans le fichier event ou dans un fichier counter.html/counter.ts, mettre dans le HTML 2 button, un avec un + dedans et l'autre avec un - et mettre également un élément de type p ou span ou peu importe qui contiendra 0 pour le moment
2. Dans le fichier ts, créer une variable counter qui sera initialisée à 0, puis faire un querySelector pour récupérer le button avec le + dedans (vous pouvez lui rajouter un id, une classe ou ce que vous voulez pour le sélectionner plus facilement)
3. Sur cet élément, rajouter un Event Listener au click qui incrémentera la valeur de counter de 1 et qui fera un console log de sa valeur actuelle
4. Une fois que ça marche, faire en sorte de capturer l'élément p/span/autre et modifier son texte pour lui mettre la valeur de counter (toujours à l'intérieur du event listener)
5. Faire pareil pour le button - qui décrémentera le counter de 1 au click
Bonus : On peut rajouter un bouton retour à zéro. Autre bonus, faire que le increment et le decrement soit une seule et même fonction appelé par les deux event 

### Le carré qui bouge ([html](square.html)/[ts](src/square.ts)/[css](css/square.css))
1. Créer des nouveaux fichier square.html/square.ts et lier les deux fichiers
2. Dans le square.html, créer une div avec un id playground et dans cet élément mettre une div avec un id square et faire en sorte en css que le square soit un carré rouge en position absolute par exemple, et que la playground prenne l'intégralité de la page (avec des width: 100vw et height: 100vh)
3. Dans le typescript, capturer les éléments square et playground et les mettre dans des variables
4. Rajouter un event au click sur le playground, mais cette fois ci, dans les parenthèses de la fonction, on va mettre un paramètre event qui va nous permettre de récupérer la position cliquée par la souris (avec les propriétés clientX et clientY de l'event), pas hésiter à les console log pour voir les valeurs
5. Assigner la valeur du clientX et du clientY au style.top et au style.left du square en concatenant un 'px' derrière
6. Pourquoi pas rajouter une petite transition sur le square pour faire qu'il se déplace élégamment vers sa destination
Bonus: Faire en sorte que le carré suive la souris sans avoir à cliquer
Autre bonus, faire que le carré se positionne avec la souris au centre du click/déplacement plutôt qu'au coin supérieur gauche 
